
* Added "Enable Timeout" preference to let it run forever.

* Included translations in many languages.

* Improved navigation.

* Fixed crashes.