package org.fdroid.fdroid.data;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import java.util.Collections;
import java.util.List;

import androidx.annotation.Nullable;

/**
 * Placeholder to avoid modifying classes copied from fdroidclient.
 */
public class RepoProvider {

    public static class Helper {
        public static Repo get(Context context, Uri uri) {
            throw new IllegalStateException("unimplemented");
        }

        public static List<Repo> all(Context context) {
            return Collections.EMPTY_LIST; // no op
        }

        public static Repo findByAddress(Context context, String repoAddress) {
            throw new IllegalStateException("unimplemented");
        }

        public static Repo findById(Context context, long repoId) {
            throw new IllegalStateException("unimplemented");
        }

        @Nullable
        public static Repo findByUrl(Context context, Uri uri, String[] projection) {
            throw new IllegalStateException("unimplemented");
        }

        public static Uri insert(Context context, ContentValues values) {
            throw new IllegalStateException("unimplemented");
        }

        public static void remove(Context context, long repoId) {
            throw new IllegalStateException("unimplemented");
        }
    }
}
