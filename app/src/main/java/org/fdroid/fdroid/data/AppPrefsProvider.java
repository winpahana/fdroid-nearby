package org.fdroid.fdroid.data;

import android.content.Context;

import androidx.annotation.NonNull;

/**
 * Placeholder to avoid modifying classes copied from fdroidclient.
 */
public class AppPrefsProvider {

    public static final class Helper {

        @NonNull
        public static AppPrefs getPrefsOrDefault(Context context, App app) {
            return AppPrefs.createDefault();
        }
    }
}
