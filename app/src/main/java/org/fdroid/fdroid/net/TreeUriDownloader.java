package org.fdroid.fdroid.net;

import android.net.Uri;

/**
 * Placeholder to avoid modifying classes copied from fdroidclient.
 */
public class TreeUriDownloader {
    public static final String TAG = "TreeUriDownloader";

    /**
     * Whoever designed this {@link android.provider.DocumentsContract#isTreeUri(Uri) URI system}
     * was smoking crack, it escapes <b>part</b> of the URI path, but not all.
     * So crazy tricks are required.
     */
    public static final String ESCAPED_SLASH = "%2F";
}
