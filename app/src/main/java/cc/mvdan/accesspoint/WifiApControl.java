package cc.mvdan.accesspoint;

import android.content.Context;
import android.net.wifi.WifiConfiguration;

/**
 * Placeholder to avoid modifying classes copied from fdroidclient.
 */
public class WifiApControl {

    public static WifiApControl getInstance(Context context) {
        return null;
    }

    public boolean isEnabled() {
        return false;
    }

    public boolean enable() {
        return false;
    }

    public void disable() {
        // no op
    }

    public boolean isWifiApEnabled() {
        return false;
    }

    public WifiConfiguration getConfiguration() {
        return null;
    }
}
